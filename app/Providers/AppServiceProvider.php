<?php

	namespace App\Providers;

	use Illuminate\Support\ServiceProvider;
	use Illuminate\Http\Request;

	use App\Models\User;
	use App\Library\Services\UserService;
	use App\Library\Services\MerchantService;

	class AppServiceProvider extends ServiceProvider
	{
	    protected $defer = true;

	    public function boot(Request $request)
	    {
	        
	    }

	    public function register()
	    {
	        $this->app->bind('merchant_service', function ($app) {
	            $merchant_key = "";

	            if($app->request->isMethod('post')) 
	            {
	                $merchant_key = $app->request->input("merchant_key");
	            }

	            if($app->request->isMethod('get')) 
	            {
	                $merchant_key = $app->request->query("merchant_key");
	            }

	            $user_service = new UserService(new User());
	            $user_data = $user_service->getUserByMerchantKey($merchant_key);
                
	            $merchant_service = new MerchantService();
	            $merchant_service->setId($user_data[0]->id);

	            return $merchant_service;
	        });
	    }

	    public function provides()
	    {
	        return ['merchant_service'];
	    }
	}
