<?php

    namespace App\Library\Services;

    use App\Library\Bases\BaseDataService;

    use App\Models\User;
    
    class UserService extends BaseDataService
    {
        public function __construct(User $model)
        {
            $this->model = $model;
        }

        public function getUserByMerchantKey($merchant_key)
        {
            return $this->model->getUserByMerchantKey($merchant_key);
        }
    }
