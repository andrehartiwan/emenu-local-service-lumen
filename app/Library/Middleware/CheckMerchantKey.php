<?php

    namespace App\Library\Middleware;

    use Closure;
    use Format;

    use App\Library\Bases\BaseMiddleware;
    use App\Library\Services\UserService;
    use App\Library\ErrorManager\ErrorCode;
    use App\Library\ErrorManager\ErrorMessage;
    
    class CheckMerchantKey
    {
        public function __construct(UserService $service)
        {
            $this->service = $service;
        }

        public function handle($request, Closure $next)
        {
            $merchant_key = "";

            if($request->isMethod('post')) 
            {
                $merchant_key = $request->input("merchant_key");
            }

            if($request->isMethod('get')) 
            {
                $merchant_key = $request->query("merchant_key");
            }

            if(!$merchant_key)
            {
                return Format::apiResponse(false, ErrorMessage::ERR_VALIDATION, ErrorCode::ERR_VALIDATION, [], ["Invalid merchant key"], ErrorCode::ERR_VALIDATION);
            }

            $user_data = $this->service->getUserByMerchantKey($merchant_key);
                
            if($user_data->isEmpty())
            {
                return Format::apiResponse(false, "Empty response", 404, [], ["Merchant key not found"], 404);
            }

            return $next($request);
        }
    }
