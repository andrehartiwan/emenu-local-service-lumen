<?php

    namespace App\Library\Middleware;

    use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
    use Tymon\JWTAuth\Exceptions\JWTException;
    use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

    use Closure;
    use Format;

    use App\Library\ErrorManager\ErrorCode;
    use App\Library\ErrorManager\ErrorMessage;
    
    class Authenticate extends BaseMiddleware
    {
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         *
         * @throws \Tymon\JWTAuth\Exceptions\JWTException
         *
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if (!$this->auth->parser()->setRequest($request)->hasToken()) {
                return Format::apiResponse(ErrorCode::ERR_MISSING_TOKEN, ErrorMessage::ERR_MISSING_TOKEN, [], 401);
            }

            try {
                if (!$this->auth->parseToken()->authenticate()) {
                    return Format::apiResponse(ErrorCode::ERR_USER_NOT_FOUND, ErrorMessage::ERR_USER_NOT_FOUND, [], 401);
                }
            } catch (JWTException $e) {
                return Format::apiResponse($e->getCode(), $e->getMessage(), [], 401);
            }

            return $next($request);
        }
    }
