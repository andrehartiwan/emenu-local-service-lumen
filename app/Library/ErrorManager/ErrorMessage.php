<?php
    
    namespace App\Library\ErrorManager;

    class ErrorMessage
	{
		const ERR_MISSING_TOKEN = 'Token not found';
        const ERR_USER_NOT_FOUND = 'User not found';
        const ERR_LOGIN_INVALID = 'Login invalid';
        const ERR_VALIDATION = 'Invalid Data Sent';
	}