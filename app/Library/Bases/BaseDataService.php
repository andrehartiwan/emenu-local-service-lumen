<?php
	
	namespace App\Library\Bases;
	
	abstract class BaseDataService
	{
		protected $model;
		protected $id;

		public function setId($id)
		{
			$this->id = $id;
		}

		public function getId()
		{
			return $this->id;
		}
	}