<?php
	
	namespace App\Library\Bases;
	
	interface ProcessorInterface
	{
		public function process($data, $model);
		public function getErrorMessage();
		public function getOutput();
	}