<?php
	
	namespace App\Library\Bases;
	
	use Laravel\Lumen\Application;

	abstract class BaseModuleRoute
	{
		protected $controller_ns;
        protected $route_prefix;
        
        const GLOBAL_PREFIX = 'emenusvc';

        abstract public function bind(Application $app);
	}