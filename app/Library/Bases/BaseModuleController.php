<?php

	namespace App\Library\Bases;

	use App\Http\Controllers\Controller;

    use App\Library\ErrorManager\ErrorMessage;
    use App\Library\ErrorManager\ErrorCode;

    use Format;

	class BaseModuleController extends Controller
	{
	    protected function startProcess($input, $form_processor, $data_processor, $service)
	    {
            if($input->isMethod('get') || $input->isMethod('post'))
            {
                if($form_processor->validate($input) == false)
                {
                    //return Format::apiResponse(ErrorCode::ERR_VALIDATION, Format::ioObject('errors', $form_processor->getErrors()), [], ErrorCode::ERR_VALIDATION);
                    return Format::apiResponse(false, ErrorMessage::ERR_VALIDATION, ErrorCode::ERR_VALIDATION, [], $form_processor->getErrors(), ErrorCode::ERR_VALIDATION);
                }
                else
                {
                    if($data_processor->process($form_processor->getData(), $service) == false)
                    {
                        return Format::apiResponse(false, $data_processor->getErrorMessage(), $data_processor->getErrorCode(), [], null, $data_processor->getErrorCode());
                    }

                    return Format::apiResponse(true, 'Success', $data_processor->getSuccessCode(), $data_processor->getOutput(), null, 200);
                }
            }
	    }
	}
