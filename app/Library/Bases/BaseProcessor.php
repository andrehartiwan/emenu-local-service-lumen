<?php
	
	namespace App\Library\Bases;
	
	abstract class BaseProcessor implements ProcessorInterface
	{
		protected $error_message;
		protected $error_code;
		protected $output;
		protected $success_code;
		
		abstract function process($data, $model);

		public function getErrorMessage()
                {
        	       return $this->error_message;
                }

                public function getOutput()
                {
                	return $this->output;
                }

                public function getErrorCode()
                {
                	return $this->error_code;
                }

                public function getSuccessCode()
                {
                	return $this->success_code;
                }
	}