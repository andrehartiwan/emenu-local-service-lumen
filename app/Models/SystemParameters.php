<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class SystemParameters extends Model
	{
	    public function getSystemParameter($id, $created_by)
	    {
	    	$result = $this->where('identifier', $id)->where('created_by', $created_by)->get();
	    	return $result;
	    }
	}