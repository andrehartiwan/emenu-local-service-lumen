<?php

	namespace App\Modules\LocalService\MasterData\Controllers;

	use Illuminate\Http\Request;

	use App\Library\Bases\BaseModuleController;

	use App\Modules\LocalService\MasterData\Services\MasterDataService;

	use App\Modules\LocalService\MasterData\Requests\MasterDataRepository;
	use App\Modules\LocalService\MasterData\Processors\MasterDataProcessor;

	class MasterDataController extends BaseModuleController
	{
		public function getMasterData(Request $request, MasterDataRepository $form_processor, MasterDataProcessor $data_processor, MasterDataService $service)
	    {
	    	//$merchant_service = app()->make('merchant_service');
	    	$form_processor->setOperation('get_master_data');
	    	return $this->startProcess($request, $form_processor, $data_processor, $service);
	    }
	}
