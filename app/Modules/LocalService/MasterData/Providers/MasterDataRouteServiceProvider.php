<?php

    namespace App\Modules\LocalService\MasterData\Providers;

    use App\Modules\LocalService\MasterData\RouteRegistry\MasterDataRoutes;

    use Illuminate\Support\ServiceProvider;

    class MasterDataRouteServiceProvider extends ServiceProvider
    {
        public function register()
        {
            $registry = $this->app->make(MasterDataRoutes::class);

            if (!is_object($registry)) {
                Log::info('Not adding any service routes - route file is missing');
                return;
            }
          
            $registry->bind(app());
        }
    }
