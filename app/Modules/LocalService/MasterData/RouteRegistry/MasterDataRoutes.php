<?php
    
    namespace App\Modules\LocalService\MasterData\RouteRegistry;

    use Laravel\Lumen\Application;

    use App\Library\Bases\BaseModuleRoute;

    class MasterDataRoutes extends BaseModuleRoute
    {
        public function __construct()
        {
            $this->controller_ns = 'App\Modules\LocalService\MasterData\Controllers';
            $this->route_prefix = BaseModuleRoute::GLOBAL_PREFIX;
        }

        public function bind(Application $app)
        {
            $app->router->group(['prefix' => $this->route_prefix, 'namespace' => $this->controller_ns], function () use ($app) {
                    
                $app->router->get('get_master_data', [
                    'as' => $this->route_prefix . '.get_master_data', 
                    'uses' => 'MasterDataController@getMasterData',
                    'middleware' => 'check.merchant.key'
                ]);

            });
        }
    }