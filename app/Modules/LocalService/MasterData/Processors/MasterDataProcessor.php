<?php

	namespace App\Modules\LocalService\MasterData\Processors;

	use App\Library\Bases\BaseProcessor;

	class MasterDataProcessor extends BaseProcessor
	{
		public function process($data, $service)
		{
			try
			{
		    	switch($data['op_type'])
		    	{
		    		case 'get_master_data':
		    			$this->output = $service->getMasterData($data);
		    			break;
		    	}

		    	return true;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				$this->error_code = $e->getCode();
				return false;
			}

			/*
			switch($data['op_type'])
	    	{
	    		case 'get_master_data':
	    			$this->output = $service->getMasterData($data);
	    			break;
	    	}

	    	return true;
	    	*/
		}
	}
