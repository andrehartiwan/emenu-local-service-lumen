<?php

    namespace App\Modules\LocalService\MasterData\Requests;

    use App\Library\Bases\BaseRepository;

    class MasterDataRepository extends BaseRepository
    {
        private $merchant_key;
        private $version_key;

        public function setOperation($operation_type)
        {
            $this->operation_type = $operation_type;
        }

        public function getInput($request)
        {
            $this->merchant_key = $request->input('merchant_key');
            $this->version_key = $request->input('version_key');
        }

        public function setValidationData()
        {
            $this->data = [
                'merchant_key' => $this->merchant_key,
                'version_key' => $this->version_key,
                'op_type' => $this->operation_type,
            ];
        }

        public function setValidationRules()
        {
            switch($this->operation_type)
            {
                case 'get_master_data':

                    $this->rules = [
                        'merchant_key' => 'required',
                        'version_key' => 'required',
                    ];

                    break;
            }
        }

    }
